# README #

The terminology I use below means the following:  
game - The application itself.  
session - A single game instance.  

## Main code-modules
1. LevelCreator scripts.  
2. Input Mapper scripts.  
3. Model-Controller scripts.  
4. SessionController script.  
5. UIController scripts.  

### Here are how they interact
1. LevelCreator scripts help make levels.
2. Input Mapper receives input from the device and relays it to the modules who have subscribed to its events. This helps centralizing the input mappings and extending/re-mapping keys becomes easy.
3. Model-Controller scripts are the "entities" for a specific session.
4. SessionController is the mediator between Model-Controllers and UIControllers. It is also the entry point to the session (instantiates a session based on LevelData from LevelCreator).
5. UIController scripts handle the UI.

The game right now works best for 1920x1080 only.

## Input bindings for (Player 1) & (Player 2)
1. Movement (W,A,S,D) & (Numpad 5,1,2,3)
2. Pickup (Q) & (Numpad 4)
3. Place (E) & (Numpad 6)

The behaviour of Pickup and Place depends on the context.  
Pickup from side-tables aka IngredientStock - Generates a new ingredients and picks it up (only if chef is holding less than 2 ingredients)  
Pickup from Regular plate - Picks up the ingredient already placed on the plate (only if chef is holding less than 2 ingredients)  
Pickup from Chopping board - Can only pick up a combination (dish) from the chopping board, that too if the chef isn't holding anything.  

Place on Regular plate - Places an ingredient already held by the chef.  
Place on chopping board - Places an ingredient already help by the chef and starts chopping.  
Place on Customer Plate - Can only place a dish on the customer plate, not individual ingredients. Once a dish is placed, it is automatically served to the customer.  

## What I did additionally
1. The ability to customize a level. Can play both single and double player.
2. Rebindable keys.
3. When a customer is angry, they turn "red".
4. The chef turns translucent whenever it is holding ingredients or dish.

## Next few steps (features)
1. Include animations for chopping, eating etc.
2. Add in different types of ingredients eg. fruits, sauces
3. Add "cooking" along with "chopping".

## Next few steps (programming)
1. Make the InputMapper pipeline more sturdy.
2. Be able to navigate the whole game with just keyboard.
3. Save game file.
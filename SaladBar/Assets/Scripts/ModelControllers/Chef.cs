﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chef : MonoBehaviour
{
    private string m_name;
    private float m_movementSpeed;
    private float m_choppingSpeed;
    private int m_ingredientCarryCapacity;
    private float m_totalTimeGiven;
    private Bar m_bar;

    private Bounds m_walkBounds;
    private float m_timeLeft;
    private int m_score;
    private bool m_isChopping;
    private List<Ingredient> m_carriedIngredients;
    private Dish m_carriedDish;
    private SpriteRenderer m_spriteRenderer;

    public string Name { get => m_name; }
    public float MovementSpeed { get => m_movementSpeed; }
    public float ChoppingSpeed { get => m_choppingSpeed; }
    public int IngredientCarryCapacity { get => m_ingredientCarryCapacity; }
    public float TotalTimeGiven { get => m_totalTimeGiven; }

    public float TimeLeft { get => m_timeLeft; }
    public int Score { get => m_score; }
    public bool IsChopping { get => m_isChopping; }

    private void Update()
    {
        m_timeLeft -= Time.deltaTime;
        if (m_timeLeft < 0)
            m_timeLeft = 0;
    }

    public void Init(string name, float movementSpeed, float choppingSpeed, int ingredientCarryCapacity, float totalTimeGiven, Bar bar)
    {
        m_name = name;
        m_movementSpeed = movementSpeed;
        m_choppingSpeed = choppingSpeed;
        m_ingredientCarryCapacity = ingredientCarryCapacity;
        m_totalTimeGiven = totalTimeGiven;

        m_bar = bar;
        m_walkBounds = bar.WalkBounds;
        m_timeLeft = m_totalTimeGiven;
        m_score = 0;
        m_carriedIngredients = new List<Ingredient>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private IEnumerator SpeedX(float multiplier, float time)
    {
        m_movementSpeed *= multiplier;
        m_choppingSpeed /= multiplier;
        yield return new WaitForSeconds(time);
        m_movementSpeed /= multiplier;
        m_choppingSpeed *= multiplier;
    }

    private void MoveToPosition(Vector3 position)
    {
        if (!m_walkBounds.Contains(position))
        {
            position = m_walkBounds.ClosestPoint(position);
        }
        transform.position = position;
    }

    private void UpdateDisplay()
    {
        List<Ingredient> ingredients;
        if (m_carriedDish != null)
        {
            ingredients = m_carriedDish.Ingredients;
        }
        else
        {
            ingredients = m_carriedIngredients;
        }

        int totalIngredients = ingredients.Count;
        Vector3 offset = new Vector3(0.0f, -0.5f, 0.0f);
        Vector3 delta = new Vector3(0.5f, 0.0f, 0.0f);
        Vector3 width = delta * (totalIngredients - 1);
        Vector3 current = offset + (-width * 0.5f);
        Vector3 scale = new Vector3(0.75f, 0.75f, 0.75f);
        for (int i = 0; i < totalIngredients; i++)
        {
            ingredients[i].transform.SetParent(transform);
            ingredients[i].transform.localPosition = current;
            ingredients[i].transform.localScale = scale;
            current += delta;
        }

        Color color = m_spriteRenderer.color;
        if (totalIngredients > 0)
        {
            color.a = 0.5f;
        }
        else
        {
            color.a = 1.0f;
        }
        m_spriteRenderer.color = color;
    }

    private IEnumerator Chopping(IChoppable ingredient, float time)
    {
        m_isChopping = true;
        yield return new WaitForSeconds(time);
        m_isChopping = false;
        ingredient.SetChopped(true);
    }

    public void AddScore(int amount)
    {
        m_score += amount;
    }

    public void AddTime(float amount)
    {
        m_timeLeft += amount;
    }

    public void AddSpeed(float multiplier, float time)
    {
        StartCoroutine(SpeedX(multiplier, time));
    }

    public void MoveLeft()
    {
        if (!m_isChopping && m_timeLeft > 0.0f)
        {
            Vector3 position = transform.position + (Vector3.left * m_movementSpeed * Time.deltaTime);
            MoveToPosition(position);
        }
    }

    public void MoveRight()
    {
        if (!m_isChopping && m_timeLeft > 0.0f)
        {
            Vector3 position = transform.position + (Vector3.right * m_movementSpeed * Time.deltaTime);
            MoveToPosition(position);
        }
    }

    public void MoveUp()
    {
        if (!m_isChopping && m_timeLeft > 0.0f)
        {
            Vector3 position = transform.position + (Vector3.up * m_movementSpeed * Time.deltaTime);
            MoveToPosition(position);
        }
    }

    public void MoveDown()
    {
        if (!m_isChopping && m_timeLeft > 0.0f)
        {
            Vector3 position = transform.position + (Vector3.down * m_movementSpeed * Time.deltaTime);
            MoveToPosition(position);
        }
    }

    public void Pickup()
    {
        if (m_isChopping || m_carriedDish != null ||  m_timeLeft <= 0.0f)
        {
            return;
        }

        InventoryItem nearestInventory = m_bar.GetNearestInventoryItem(transform.position);
        if (nearestInventory != null)
        {
            switch (nearestInventory.kind)
            {
                case InventoryItem.Kind.PLATE:
                case InventoryItem.Kind.INGREDIENT_STOCK:
                    {
                        if (m_carriedIngredients.Count == m_ingredientCarryCapacity)
                        {
                            return;
                        }
                        Ingredient.Kind nearestIngredientKind = m_bar.GetNearestIngredientKind(transform.position);
                        foreach (Ingredient carryIngredient in m_carriedIngredients)
                        {
                            if (carryIngredient.kind == nearestIngredientKind)
                            {
                                return;
                            }
                        }

                        Ingredient ingredient = m_bar.GetNearestIngredient(transform.position);
                        if (ingredient != null)
                        {
                            m_carriedIngredients.Add(ingredient);
                        }
                    }
                    break;

                case InventoryItem.Kind.CHOPPING_BOARD:
                    {
                        if (m_carriedIngredients.Count > 0)
                        {
                            return;
                        }
                        m_carriedDish = ((ChoppingBoard)nearestInventory).RemoveDish();
                    }
                    break;
            }
        }

        UpdateDisplay();
    }

    public void Place()
    {
        if (m_isChopping || m_timeLeft <= 0.0f)
        {
            return;
        }

        InventoryItem inventoryItem = m_bar.GetNearestInventoryItem(transform.position);
        if (m_carriedDish != null)
        {
            if (inventoryItem is CustomerPlate)
            {
                CustomerPlate customerPlate = (CustomerPlate)inventoryItem;
                customerPlate.AddDish(m_carriedDish);
                m_carriedDish = null;
                m_bar.ServePlateToCustomer(customerPlate, this);
            }
            else if (inventoryItem is Dustbin)
            {
                Dustbin dustbin = (Dustbin)inventoryItem;
                dustbin.AddDish(m_carriedDish);
                m_carriedDish = null;
            }
        }
        else if (m_carriedIngredients.Count > 0)
        {
            Ingredient ingredient = m_carriedIngredients[0];

            if (inventoryItem is RegularPlate)
            {
                RegularPlate plate = (RegularPlate)inventoryItem;
                if (plate.AddIngredient(ingredient))
                    m_carriedIngredients.RemoveAt(0);
            }
            else if (inventoryItem is ChoppingBoard)
            {
                ChoppingBoard choppingBoard = (ChoppingBoard)inventoryItem;
                choppingBoard.AddIngredient(ingredient);
                m_carriedIngredients.RemoveAt(0);

                if (ingredient is IChoppable)
                {
                    StartCoroutine(Chopping((IChoppable)ingredient, m_choppingSpeed));
                }
            }
            else if (inventoryItem is Dustbin)
            {
                Dustbin dustbin = (Dustbin)inventoryItem;
                dustbin.AddIngredient(ingredient);
                m_carriedIngredients.RemoveAt(0);
            }
        }

        UpdateDisplay();
    }
}

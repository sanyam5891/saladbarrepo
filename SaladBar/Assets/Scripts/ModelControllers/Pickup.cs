﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public enum Type
    {
        ADD_TIME,
        ADD_SCORE,
        ADD_SPEED,
        COUNT
    };

    private Chef m_chef;
    private Type m_type;
    private float m_totalTimeAvailable;

    private float m_timeLeft;
    private Bounds m_bounds;
    private SpriteRenderer m_spriteRenderer;

    public List<Sprite> sprites;

    private void Update()
    {
        if (m_timeLeft <= 0.0f)
            Destroy(gameObject);

        m_timeLeft -= Time.deltaTime;
    }

    public void Init(Chef chef, Type type, float totalTimeAvailable)
    {
        m_chef = chef;
        m_type = type;
        m_totalTimeAvailable = totalTimeAvailable;

        m_timeLeft = totalTimeAvailable;
        BoxCollider2D box = GetComponent<BoxCollider2D>();
        m_bounds = new Bounds(box.transform.position + new Vector3(box.offset.x, box.offset.y, 0.0f), new Vector3(box.size.x, box.size.y, 1.0f));
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        m_spriteRenderer.sprite = sprites[(int)m_type];
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Chef chef = collision.GetComponent<Chef>();
        if (chef != null && chef == m_chef)
        {
            switch (m_type)
            {
                case Type.ADD_TIME:
                    m_chef.AddTime(30.0f);
                    break;

                case Type.ADD_SCORE:
                    m_chef.AddScore(30);
                    break;

                case Type.ADD_SPEED:
                    m_chef.AddSpeed(2.0f, 30.0f);
                    break;
            }

            Destroy(gameObject);
        }
    }
}

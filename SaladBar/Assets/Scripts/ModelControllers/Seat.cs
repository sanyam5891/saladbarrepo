﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seat : MonoBehaviour
{
    [SerializeField]
    private Chair m_chair;
    [SerializeField]
    private CustomerPlate m_plate;
    [SerializeField]
    private SpriteRenderer pfIngredientOrdered;

    private List<SpriteRenderer> m_ingredientSprites;
    private bool m_isOccupied;

    public bool IsOccupied { get => m_isOccupied; }
    public Chair Chair { get => m_chair; }
    public CustomerPlate Plate { get => m_plate; }

    public void SetOccupancy(bool isOccupied)
    {
        m_isOccupied = isOccupied;
        m_chair.gameObject.SetActive(!isOccupied);
    }

    public void DisplayDish(DishRecipe dishRecipe)
    {
        if(m_ingredientSprites == null)
        {
            m_ingredientSprites = new List<SpriteRenderer>();
        }

        int totalIngredients = dishRecipe.TotalIngredients;
        Vector3 spacing = new Vector3(0.25f, 0.0f, 0.0f);
        Vector3 width = spacing * (totalIngredients - 1);
        Vector3 current = m_plate.transform.localPosition + new Vector3(0.0f, -1.0f, 0.0f) + (width * -0.5f);
        Vector3 scale = new Vector3(0.5f, 0.5f, 0.5f);
        List<Ingredient.Kind> ingredientKinds = dishRecipe.IngredientKinds;
        for (int i = 0; i < totalIngredients; i++)
        {
            SpriteRenderer spriteRenderer = Instantiate(pfIngredientOrdered, transform);
            spriteRenderer.sprite = IngredientSpriteManager.Instance.GetIngredientSprite(ingredientKinds[i]);
            spriteRenderer.transform.localPosition = current;
            spriteRenderer.transform.localScale = scale;
            current += spacing;
            m_ingredientSprites.Add(spriteRenderer);
        }
    }

    public void Free()
    {
        SetOccupancy(false);
        int totalIngredientsOrdered = m_ingredientSprites.Count;
        for(int i = 0; i < totalIngredientsOrdered; i++)
        {
            Destroy(m_ingredientSprites[i].gameObject);
        }

        m_ingredientSprites.Clear();
        m_plate.DestroyAllIngredients();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
    public enum Feedback
    {
        UNFED,
        SERVED_QUICKLY,
        SERVED_WITHIN_TIME,
        ANGRY
    };

    private Bar m_bar;
    private float m_maxWaitTime;
    private Seat m_seat;

    private float m_totalWaitTime;
    private float m_waitTimeLeft;
    private Feedback m_feedback;
    private DishRecipe m_dishRecipeOrdered;
    private Coroutine m_waitingForDish;
    private SpriteRenderer m_spriteRenderer;

    public float MaxWaitTime { get => m_maxWaitTime; }
    public Seat Seat { get => m_seat; }

    public float TotalWaitTime { get => m_totalWaitTime; }
    public float WaitTimeLeft { get => m_waitTimeLeft; }
    public Feedback feedback { get => m_feedback; }

    public void Init(Bar bar, float maxWaitTime, Seat seat)
    {
        m_bar = bar;
        m_maxWaitTime = maxWaitTime;
        m_seat = seat;

        transform.position = seat.Chair.transform.position;
        m_feedback = Feedback.UNFED;
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private IEnumerator WaitingForDish(float time)
    {
        m_waitTimeLeft = time;
        while (m_waitTimeLeft > 0)
        {
            yield return null;
            m_waitTimeLeft -= Time.deltaTime;
        }

        m_bar.RegisterFeedback(this, m_feedback);
    }

    public DishRecipe OrderDish(List<Ingredient.Kind> ingredientsAvailable, int minIngredients, int maxIngredients)
    {
        List<Ingredient.Kind> ingredientsToChooseFrom = new List<Ingredient.Kind>(ingredientsAvailable);
        int ingredientsToOrder = Random.Range(minIngredients, maxIngredients + 1);
        float timePerIngredient = m_maxWaitTime / maxIngredients;
        m_totalWaitTime = timePerIngredient * ingredientsToOrder;

        m_dishRecipeOrdered = new DishRecipe();
        for (int i = 0; i < ingredientsToOrder; i++)
        {
            int index = Random.Range(0, ingredientsToChooseFrom.Count);
            m_dishRecipeOrdered.AddIngredientKind(ingredientsToChooseFrom[index]);
            ingredientsToChooseFrom.RemoveAt(index);
        }

        if (m_waitingForDish != null)
            StopCoroutine(m_waitingForDish);
        m_waitingForDish = StartCoroutine(WaitingForDish(m_totalWaitTime));
        return m_dishRecipeOrdered;
    }

    public void CheckServedDish()
    {
        CustomerPlate plate = m_seat.Plate;
        List<Ingredient> ingredients = plate.GetIngredients();
        
        if (ingredients.Count == m_dishRecipeOrdered.TotalIngredients)
        {
            foreach (Ingredient ingredient in ingredients)
            {
                if (!m_dishRecipeOrdered.ContainsIngredientKind(ingredient.kind) ||
                    ((ingredient is IChoppable) && !((IChoppable)ingredient).IsChopped()))
                {
                    m_waitTimeLeft /= 1.5f;
                    m_spriteRenderer.color = new Color32(255, 128, 128, 255);
                    m_feedback = Feedback.ANGRY;
                    plate.RemoveDish();
                    return;
                }
            }
        }
        else
        {
            m_waitTimeLeft /= 1.5f;
            m_spriteRenderer.color = new Color32(255, 128, 128, 255);
            m_feedback = Feedback.ANGRY;
            plate.RemoveDish();
            return;
        }

        plate.RemoveDish();
        float deliverTimeRatio = m_waitTimeLeft / m_totalWaitTime;
        if (deliverTimeRatio > 0.3f)
        {
            m_feedback = Feedback.SERVED_QUICKLY;
        }
        else
        {
            m_feedback = Feedback.SERVED_WITHIN_TIME;
        }

        m_bar.RegisterFeedback(this, m_feedback);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public struct ServiceLog
{
    public Chef chef;
    public Customer customer;

    public ServiceLog(Chef chef, Customer customer)
    {
        this.chef = chef;
        this.customer = customer;
    }
}

public class Bar : MonoBehaviour
{
    private Bounds m_walkBounds;
    private List<Chef> m_chefs;
    private List<InventoryItem> m_inventoryItems;
    private List<Ingredient.Kind> m_ingredientsAvailable;
    private List<Customer> m_customers;
    private List<ProximitySensor> m_proximitySensors;
    private List<Seat> m_seats;
    private float m_customerMaxWaitTime;
    private int m_minIngredientsPerDish;
    private int m_maxIngredientsPerDish;
    private BarScreenInputMapper[] m_barScreenInputMappers;
    private List<ServiceLog> m_serviceLogs;

    public Transform[] chefSpawnLocations;
    public Transform[] ingredientStockContainers;
    public Transform seatsContainer;
    public Transform customerContainer;
    public Transform[] cookingInventoryItemContainers;
    public Transform proximitySensorContainer;
    public Dustbin dustbin;
    public Chef pfChef;
    public Seat pfSeat;
    public Customer pfCustomer;
    public Pickup pfPickup;
    public BoxCollider2D pickupSpawnArea;
    public BoxCollider2D chefWalkArea;

    public Action<Customer> onCustomerEntered, onCustomerLeft;
    public Action onGameOver;

    public List<Chef> Chefs { get => m_chefs; }
    public List<Customer> Customers { get => m_customers; }
    public Bounds WalkBounds { get => m_walkBounds; }

    private void OnDestroy()
    {
        // Unsubscribe all events from BarScreenInputMapper.
        if (m_barScreenInputMappers != null)
        {
            int chefCount = m_chefs.Count;
            int inputMapperCount = m_barScreenInputMappers.Length;
            for (int c = 0; c < chefCount; c++)
            {
                BarScreenInputMapper inputMapper = m_barScreenInputMappers[c % inputMapperCount];
                Chef chef = m_chefs[c];

                inputMapper.onMoveLeft.onStay -= chef.MoveLeft;
                inputMapper.onMoveRight.onStay -= chef.MoveRight;
                inputMapper.onMoveUp.onStay -= chef.MoveUp;
                inputMapper.onMoveDown.onStay -= chef.MoveDown;
                inputMapper.onPick.onEnter -= chef.Pickup;
                inputMapper.onPlace.onEnter -= chef.Place;
            }
        }
    }

    /// <summary>
    /// Checks from GameOver condition.
    /// Also generates customers.
    /// </summary>
    /// <returns></returns>
    private IEnumerator GameLoop()
    {
        bool timerRanOut = false;
        while (!timerRanOut)
        {
            yield return null;
            if (m_customers.Count < m_seats.Count && UnityEngine.Random.Range(0.0f, 1.0f) < 0.02f)
            {
                GenerateCustomer();
            }

            timerRanOut = true;
            foreach (Chef chef in m_chefs)
            {
                if (chef.TimeLeft > 0.0f)
                {
                    timerRanOut = false;
                    break;
                }
            }
        }

        onGameOver?.Invoke();
    }

    private void GenerateCustomer()
    {
        Customer customer = Instantiate(pfCustomer, customerContainer);
        foreach (Seat seat in m_seats)
        {
            if (!seat.IsOccupied)
            {
                customer.Init(this, m_customerMaxWaitTime, seat);
                seat.SetOccupancy(true);
                break;
            }
        }

        DishRecipe dishRecipe = customer.OrderDish(m_ingredientsAvailable, m_minIngredientsPerDish, m_maxIngredientsPerDish);
        customer.Seat.DisplayDish(dishRecipe);
        m_customers.Add(customer);
        onCustomerEntered?.Invoke(customer);
    }

    private void RemoveCustomer(Customer customer)
    {
        onCustomerLeft?.Invoke(customer);
        customer.Seat.Free();
        m_customers.Remove(customer);

        int serviceLogEntryCount = m_serviceLogs.Count;
        for(int i = 0; i < serviceLogEntryCount; i++)
        {
            if(m_serviceLogs[i].customer == customer)
            {
                m_serviceLogs.RemoveAt(i);
                i--;
                serviceLogEntryCount--;
            }
        }

        Destroy(customer.gameObject);
    }

    public void Begin()
    {
        StartCoroutine(GameLoop());
    }

    public void Init(LevelData levelData, BarScreenInputMapper[] barScreenInputMappers)
    {
        int chefCount = levelData.chefs.Count;
        BarData barData = levelData.bar;

        // Initialize fields.
        m_customerMaxWaitTime = levelData.m_customerMaxWaitTime;
        m_minIngredientsPerDish = levelData.m_minIngredientsPerDish;
        m_maxIngredientsPerDish = levelData.m_maxIngredientsPerDish;
        m_customers = new List<Customer>();

        // Initialize walk bounds.
        m_walkBounds = new Bounds(chefWalkArea.transform.position + new Vector3(chefWalkArea.offset.x, chefWalkArea.offset.y, 0.0f),
            new Vector3(chefWalkArea.size.x, chefWalkArea.size.y, 1.0f));

        // Initialize chefs.
        m_chefs = new List<Chef>();
        for (int i = 0; i < chefCount; i++)
        {
            ChefData chefData = levelData.chefs[i];
            Chef chef = Instantiate(pfChef, transform);
            chef.Init(chefData.name, chefData.movementSpeed, chefData.choppingSpeed, chefData.ingredientCarryCapacity, levelData.m_timePerChef, this);
            chef.transform.position = chefSpawnLocations[i].position;
            m_chefs.Add(chef);
        }

        // Assign input mapper roles.
        m_barScreenInputMappers = barScreenInputMappers;
        int inputMapperCount = barScreenInputMappers.Length;
        for (int c = 0; c < chefCount; c++)
        {
            BarScreenInputMapper inputMapper = barScreenInputMappers[c % inputMapperCount];
            Chef chef = m_chefs[c];

            // Unsubscribe first to avoid duplicate subsriptions.
            inputMapper.onMoveLeft.onStay -= chef.MoveLeft;
            inputMapper.onMoveRight.onStay -= chef.MoveRight;
            inputMapper.onMoveUp.onStay -= chef.MoveUp;
            inputMapper.onMoveDown.onStay -= chef.MoveDown;
            inputMapper.onPick.onEnter -= chef.Pickup;
            inputMapper.onPlace.onEnter -= chef.Place;

            // Subscribe all events from BarScreenInputMapper.
            inputMapper.onMoveLeft.onStay += chef.MoveLeft;
            inputMapper.onMoveRight.onStay += chef.MoveRight;
            inputMapper.onMoveUp.onStay += chef.MoveUp;
            inputMapper.onMoveDown.onStay += chef.MoveDown;
            inputMapper.onPick.onEnter += chef.Pickup;
            inputMapper.onPlace.onEnter += chef.Place;
        }

        // Initialize inventory items.
        m_inventoryItems = new List<InventoryItem>();
        m_ingredientsAvailable = new List<Ingredient.Kind>(barData.ingredientTypes);
        int ingredientCount = m_ingredientsAvailable.Count;
        int spotNumber = 0;
        // Initialize inventory items (Ingredient Stock).
        for (int i = 0; i < ingredientStockContainers.Length; i++)
        {
            Transform ingredientStockContainer = ingredientStockContainers[i];
            IngredientStock[] ingredientStocks = ingredientStockContainer.GetComponentsInChildren<IngredientStock>();
            for (int j = 0; j < ingredientStocks.Length; j++)
            {
                IngredientStock ingredientStock = ingredientStocks[j];
                if (spotNumber < ingredientCount)
                {
                    ingredientStock.Init(InventoryItem.Kind.INGREDIENT_STOCK, m_ingredientsAvailable[spotNumber]);
                    ingredientStock.gameObject.SetActive(true);
                    m_inventoryItems.Add(ingredientStock);
                }
                else
                {
                    ingredientStock.gameObject.SetActive(false);
                }

                spotNumber++;
            }
        }

        // Initialize inventory items (customer specific)
        m_seats = new List<Seat>();
        int seatingCapacity = barData.seatingCapacity;
        Vector3 spacing = new Vector3(2, 0, 0);
        Vector3 width = spacing * (seatingCapacity - 1);
        Vector3 current = (-width * 0.5f);
        for (int s = 0; s < seatingCapacity; s++)
        {
            Seat seat = Instantiate(pfSeat, seatsContainer);
            seat.SetOccupancy(false);
            seat.transform.localPosition = current;
            Chair chair = seat.GetComponentInChildren<Chair>();
            CustomerPlate plate = seat.GetComponentInChildren<CustomerPlate>();
            chair.Init(InventoryItem.Kind.CHAIR);
            plate.Init(InventoryItem.Kind.PLATE);
            m_inventoryItems.Add(chair);
            m_inventoryItems.Add(plate);
            m_seats.Add(seat);

            current += spacing;
        }

        // Initialize inventory items (cooking inventory)
        for (int c = 0; c < cookingInventoryItemContainers.Length; c++)
        {
            Transform cookingInventoryItemContainer = cookingInventoryItemContainers[c];
            if (c < chefCount)
            {
                cookingInventoryItemContainer.gameObject.SetActive(true);
                ChoppingBoard choppingBoard = cookingInventoryItemContainer.GetComponentInChildren<ChoppingBoard>();
                RegularPlate plate = cookingInventoryItemContainer.GetComponentInChildren<RegularPlate>();
                choppingBoard.Init(InventoryItem.Kind.CHOPPING_BOARD);
                plate.Init(InventoryItem.Kind.PLATE);
                m_inventoryItems.Add(choppingBoard);
                m_inventoryItems.Add(plate);
            }
            else
            {
                cookingInventoryItemContainer.gameObject.SetActive(false);
            }
        }

        if (dustbin)
        {
            dustbin.Init(InventoryItem.Kind.DUSTBIN);
            m_inventoryItems.Add(dustbin);
        }

        // Initialize proximity sensors.
        m_proximitySensors = new List<ProximitySensor>();
        List<ProximitySensor> proximitySensors = new List<ProximitySensor>(proximitySensorContainer.GetComponentsInChildren<ProximitySensor>());
        foreach (ProximitySensor sensor in proximitySensors)
        {
            sensor.gameObject.SetActive(false);
        }

        foreach (InventoryItem inventoryItem in m_inventoryItems)
        {
            if (!(inventoryItem is Table) && !(inventoryItem is Chair) && proximitySensors.Count > 0)
            {
                float minDistance = int.MaxValue;
                ProximitySensor nearestSensor = null;
                foreach (ProximitySensor proximitySensor in proximitySensors)
                {
                    float distance = Vector2.Distance(proximitySensor.transform.position, inventoryItem.transform.position);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        nearestSensor = proximitySensor;
                    }
                }
                if (nearestSensor != null)
                {
                    nearestSensor.item = inventoryItem;
                    m_proximitySensors.Add(nearestSensor);
                    nearestSensor.gameObject.SetActive(true);
                }
            }
        }

        m_serviceLogs = new List<ServiceLog>();
    }

    private void SpawnPickup(Chef chef)
    {
        Vector3 minSpawnPos = pickupSpawnArea.bounds.min;
        Vector3 maxSpawnPos = pickupSpawnArea.bounds.max;

        Vector3 spawnPos = new Vector3(UnityEngine.Random.Range(minSpawnPos.x, maxSpawnPos.x),
            UnityEngine.Random.Range(minSpawnPos.y, maxSpawnPos.y),
            UnityEngine.Random.Range(minSpawnPos.z, maxSpawnPos.z));

        int number = UnityEngine.Random.Range(0, (int)Pickup.Type.COUNT);
        Pickup pickup = Instantiate(pfPickup, transform);
        pickup.transform.position = spawnPos;
        pickup.Init(chef, (Pickup.Type)number, 15.0f);
    }

    private Chef GetLastServingChefForCustomer(Customer customer)
    {
        int serviceLogCount = m_serviceLogs.Count;
        for(int i = serviceLogCount-1; i >= 0; i--)
        {
            if(m_serviceLogs[i].customer == customer)
            {
                return m_serviceLogs[i].chef;
            }
        }

        return null;
    }

    private List<Chef> GetServingChefsForCustomer(Customer customer)
    {
        List<Chef> chefs = new List<Chef>();
        foreach(ServiceLog serviceLog in m_serviceLogs)
        {
            if(serviceLog.customer == customer && !chefs.Contains(serviceLog.chef))
            {
                chefs.Add(serviceLog.chef);
            }
        }
        return chefs;
    }

    public InventoryItem GetNearestInventoryItem(Vector3 position)
    {
        InventoryItem inventoryItem = null;
        for (int i = 0; i < m_proximitySensors.Count; i++)
        {
            if (m_proximitySensors[i].IsInside(position))
            {
                inventoryItem = m_proximitySensors[i].GetInventoryItem();
                break;
            }
        }
        return inventoryItem;
    }

    public Ingredient.Kind GetNearestIngredientKind(Vector3 position)
    {
        InventoryItem inventoryItem = GetNearestInventoryItem(position);
        if (inventoryItem)
        {
            InventoryItem.Kind inventoryKind = inventoryItem.kind;
            switch (inventoryKind)
            {
                case InventoryItem.Kind.INGREDIENT_STOCK:
                    IngredientStock ingredientStock = (IngredientStock)inventoryItem;
                    return ingredientStock.ingredientKind;

                case InventoryItem.Kind.PLATE:
                    {
                        if (inventoryItem is RegularPlate)
                        {
                            RegularPlate regularPlate = (RegularPlate)inventoryItem;
                            Ingredient ingredient = regularPlate.GetIngredient(0);
                            if (ingredient != null)
                                return ingredient.kind;
                        }
                    }
                    break;
            }
        }

        return 0;
    }

    public Ingredient GetNearestIngredient(Vector3 position)
    {
        Ingredient ingredient = null;
        InventoryItem inventoryItem = GetNearestInventoryItem(position);

        if (inventoryItem == null)
        {
        }
        else
        {
            InventoryItem.Kind inventoryKind = inventoryItem.kind;
            switch (inventoryKind)
            {
                case InventoryItem.Kind.INGREDIENT_STOCK:
                    IngredientStock ingredientStock = (IngredientStock)inventoryItem;
                    ingredient = ingredientStock.GetIngredient();
                    break;

                case InventoryItem.Kind.PLATE:
                    {
                        if (inventoryItem is RegularPlate)
                        {
                            RegularPlate regularPlate = (RegularPlate)inventoryItem;
                            ingredient = regularPlate.RemoveIngredient(0);
                        }
                    }
                    break;
            }
        }

        return ingredient;
    }

    public void ServePlateToCustomer(CustomerPlate customerPlate, Chef chef)
    {
        Customer customerToServe = null;
        foreach (Customer customer in m_customers)
        {
            if (customer.Seat.Plate == customerPlate)
            {
                customerToServe = customer;
                break;
            }
        }

        if (customerToServe != null)
        {
            ServiceLog serviceLog = new ServiceLog(chef, customerToServe);
            m_serviceLogs.Add(serviceLog);
            customerToServe.CheckServedDish();
        }
    }

    public void RegisterFeedback(Customer customer, Customer.Feedback feedback)
    {
        switch (feedback)
        {
            case Customer.Feedback.UNFED:
                {
                    foreach (Chef ch in m_chefs)
                    {
                        ch.AddScore(-30);
                    }
                }
                break;

            case Customer.Feedback.ANGRY:
                {
                    List<Chef> chefs = GetServingChefsForCustomer(customer);
                    foreach(Chef chef in chefs)
                    {
                        chef.AddScore(-60);
                    }
                }
                break;

            case Customer.Feedback.SERVED_QUICKLY:
                {
                    Chef chef = GetLastServingChefForCustomer(customer);
                    chef.AddScore(30);
                    SpawnPickup(chef);
                }
                break;

            case Customer.Feedback.SERVED_WITHIN_TIME:
                {
                    Chef chef = GetLastServingChefForCustomer(customer);
                    chef.AddScore(30);
                }
                break;
        }

        RemoveCustomer(customer);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximitySensor : MonoBehaviour
{
    private Bounds m_bounds;
    public InventoryItem item;
    
    void Awake()
    {
        BoxCollider2D collider = GetComponent<BoxCollider2D>();
        m_bounds = collider.bounds;
    }

    public bool IsInside(Vector3 position)
    {
        return m_bounds.Contains(position);
    }

    public InventoryItem GetInventoryItem()
    {
        return item;
    }
}

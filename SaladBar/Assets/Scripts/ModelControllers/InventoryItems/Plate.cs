﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : InventoryItem
{
    protected List<Ingredient> m_ingredients;

    public override void Init(Kind kind)
    {
        base.Init(kind);
        m_ingredients = new List<Ingredient>();
    }

    public void UpdateDisplay()
    {
        int totalIngredients = m_ingredients.Count;
        Vector3 delta = new Vector3(0.25f, 0.0f, 0.0f);
        Vector3 width = delta * (totalIngredients - 1);
        Vector3 current = -width * 0.5f;
        Vector3 scale = new Vector3(0.5f, 0.5f, 0.5f);
        for (int i = 0; i < totalIngredients; i++)
        {
            m_ingredients[i].transform.SetParent(transform);
            m_ingredients[i].transform.localPosition = current;
            m_ingredients[i].transform.localScale = scale;
            current += delta;
        }
    }

    public void DestroyAllIngredients()
    {
        int totalIngredients = m_ingredients.Count;
        for (int i = 0; i < totalIngredients; i++)
        {
            Destroy(m_ingredients[i].gameObject);
        }
        m_ingredients.Clear();
    }

    public List<Ingredient> GetIngredients()
    {
        List<Ingredient> ingredients = new List<Ingredient>(m_ingredients);
        return ingredients;
    }
}
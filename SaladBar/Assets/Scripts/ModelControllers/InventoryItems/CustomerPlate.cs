﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerPlate : Plate
{
    private Dish m_dish;

    public Dish dish { get => m_dish; }

    public void AddDish(Dish dish)
    {
        foreach(Ingredient ingredient in dish.Ingredients)
        {
            m_ingredients.Add(ingredient);
        }
        m_dish = dish;
    }

    public void RemoveDish()
    {
        if(m_dish != null)
        {
            m_dish = null;
            DestroyAllIngredients();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularPlate : Plate
{
    public bool AddIngredient(Ingredient ingredient)
    {
        m_ingredients.Add(ingredient);
        UpdateDisplay();
        return true;
    }

    public Ingredient GetIngredient(int index)
    {
        if (m_ingredients.Count > index)
        {
            Ingredient ingredient = m_ingredients[index];
            return ingredient;
        }
        else
        {
            return null;
        }
    }

    public Ingredient RemoveIngredient(Ingredient.Kind kind)
    {
        int totalIngredients = m_ingredients.Count;
        for (int i = 0; i < totalIngredients; i++)
        {
            if (m_ingredients[i].kind == kind)
            {
                Ingredient ingredient = m_ingredients[i];
                m_ingredients.RemoveAt(i);
                UpdateDisplay();
                return ingredient;
            }
        }

        return null;
    }

    public Ingredient RemoveIngredient(int index)
    {
        if (m_ingredients.Count > index)
        {
            Ingredient ingredient = m_ingredients[index];
            m_ingredients.RemoveAt(index);
            UpdateDisplay();
            return ingredient;
        }
        else
        {
            return null;
        }
    }
}

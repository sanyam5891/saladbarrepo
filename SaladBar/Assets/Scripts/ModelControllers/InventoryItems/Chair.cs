﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chair : InventoryItem
{
    private bool m_occupied;
    private Customer m_customer;

    public bool Occupied { get => m_occupied; }
    public Customer customer { get => m_customer; }
}

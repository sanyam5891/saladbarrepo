﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoppingBoard : InventoryItem
{
    private List<Ingredient> m_ingredients;

    public override void Init(Kind kind)
    {
        base.Init(kind);
        m_ingredients = new List<Ingredient>();
    }

    public void UpdateDisplay()
    {
        int totalIngredients = m_ingredients.Count;
        Vector3 delta = new Vector3(0.25f, 0.0f, 0.0f);
        Vector3 width = delta * (totalIngredients - 1);
        Vector3 current = (width * -0.5f);
        Vector3 scale = new Vector3(0.5f, 0.5f, 0.5f);
        for (int i = 0; i < totalIngredients; i++)
        {
            m_ingredients[i].transform.SetParent(transform);
            m_ingredients[i].transform.localPosition = current;
            m_ingredients[i].transform.localScale = scale;
            current += delta;
        }
    }

    public void AddIngredient(Ingredient ingredient)
    {
        m_ingredients.Add(ingredient);
        UpdateDisplay();
    }

    public Dish RemoveDish()
    {
        Dish dish = null;
        if (m_ingredients.Count > 0)
        {
            dish = new Dish(m_ingredients);
        }

        m_ingredients.Clear();
        UpdateDisplay();
        return dish;
    }
    /*
    public Ingredient GetIngredient(int index)
    {
        if (m_ingredients.Count > index)
        {
            Ingredient ingredient = m_ingredients[index];
            return ingredient;
        }
        else
        {
            return null;
        }
    }

    public Ingredient RemoveIngredient(Ingredient.Kind kind)
    {
        int totalIngredients = m_ingredients.Count;
        for (int i = 0; i < totalIngredients; i++)
        {
            if (m_ingredients[i].kind == kind)
            {
                Ingredient ingredient = m_ingredients[i];
                m_ingredients.RemoveAt(i);
                UpdateDisplay();
                return ingredient;
            }
        }

        return null;
    }

    public Ingredient RemoveIngredient(int index)
    {
        if (m_ingredients.Count > index)
        {
            Ingredient ingredient = m_ingredients[index];
            m_ingredients.RemoveAt(0);
            UpdateDisplay();
            return ingredient;
        }
        else
        {
            return null;
        }
    }
    */

    public List<Ingredient> GetIngredients()
    {
        return m_ingredients;
    }
}
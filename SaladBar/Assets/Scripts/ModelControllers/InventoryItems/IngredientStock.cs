﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientStock : InventoryItem
{
    private Ingredient.Kind m_ingredientKind;

    public Ingredient pfIngredient;

    public Ingredient.Kind ingredientKind { get => m_ingredientKind; }

    public Ingredient GenerateIngredient(Ingredient.Kind kind)
    {
        Ingredient ingredient = Instantiate(pfIngredient, transform);
        ingredient.Init(kind);
        return ingredient;
    }

    public void Init(Kind kind, Ingredient.Kind ingredientKind)
    {
        Init(kind);
        m_ingredientKind = ingredientKind;
        GetComponent<SpriteRenderer>().sprite = IngredientSpriteManager.Instance.GetIngredientStockSprite(ingredientKind);
    }

    public Ingredient GetIngredient()
    {
        Ingredient ingredient = GenerateIngredient(m_ingredientKind);
        return ingredient;
    }
}

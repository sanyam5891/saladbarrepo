﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dustbin : InventoryItem
{
    public void AddIngredient(Ingredient ingredient)
    {
        Destroy(ingredient.gameObject);
    }

    public void AddDish(Dish dish)
    {
        int totalIngredients = dish.Ingredients.Count;
        for(int i = 0; i < totalIngredients; i++)
        {
            Destroy(dish.Ingredients[i].gameObject);
        }
    }
}

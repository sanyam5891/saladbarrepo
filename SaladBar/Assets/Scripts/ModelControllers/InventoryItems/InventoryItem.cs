﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour
{
    public enum Kind
    {
        CHOPPING_BOARD,
        PLATE,
        DUSTBIN,
        TABLE,
        CHAIR,
        INGREDIENT_STOCK
    };

    protected Kind m_kind;

    public Kind kind { get => m_kind; }

    public virtual void Init(Kind kind)
    {
        m_kind = kind;
    }
}

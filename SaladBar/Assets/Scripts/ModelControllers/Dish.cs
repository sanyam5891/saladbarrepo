﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dish
{
    private List<Ingredient> m_ingredients;

    public List<Ingredient> Ingredients { get => m_ingredients; }

    public Dish(List<Ingredient> ingredients)
    {
        m_ingredients = new List<Ingredient>(ingredients);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient : MonoBehaviour
{
    public enum Kind
    {
        Capsicum,
        Tomato,
        Onion,
        Olive,
        Cucumber,
        Broccoli
    };

    protected SpriteRenderer m_renderer;
    protected Kind m_kind;

    public Kind kind { get => m_kind; }

    public void Init(Kind kind)
    {
        m_kind = kind;
        m_renderer = GetComponent<SpriteRenderer>();
        m_renderer.sprite = IngredientSpriteManager.Instance.GetIngredientSprite(this);
    }
}

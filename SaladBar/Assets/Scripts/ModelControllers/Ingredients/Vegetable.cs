﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vegetable : Ingredient, IChoppable
{
    private bool m_chopped;

    public void SetChopped(bool chopped)
    {
        m_chopped = chopped;
        m_renderer.sprite = IngredientSpriteManager.Instance.GetIngredientSprite(this);
    }

    public bool IsChopped()
    {
        return m_chopped;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DishRecipe
{
    private List<Ingredient.Kind> m_ingredientKinds;

    public List<Ingredient.Kind> IngredientKinds { get => m_ingredientKinds; }
    public int TotalIngredients { get => IngredientKinds.Count; }

    public DishRecipe()
    {
        m_ingredientKinds = new List<Ingredient.Kind>();
    }

    public void AddIngredientKind(Ingredient.Kind kind)
    {
        if(!m_ingredientKinds.Contains(kind))
        {
            m_ingredientKinds.Add(kind);
        }
    }

    public bool ContainsIngredientKind(Ingredient.Kind kind)
    {
        return m_ingredientKinds.Contains(kind);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct IngredientSprites
{
    public Ingredient.Kind kind;
    public Sprite stock;
    public Sprite single;
    public Sprite chopped;
};

public class IngredientSpriteManager : MonoBehaviour
{
    private static IngredientSpriteManager m_instance;

    public IngredientSprites[] ingredientSprites;

    public static IngredientSpriteManager Instance
    {
        get
        {
            return m_instance;
        }
    }

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Sprite GetIngredientSprite(Ingredient.Kind kind)
    {
        for (int i = 0; i < ingredientSprites.Length; i++)
        {
            if (ingredientSprites[i].kind == kind)
            {
                return ingredientSprites[i].single;
            }
        }

        return null;
    }

    public Sprite GetIngredientSprite(Ingredient ingredient)
    {
        Ingredient.Kind kind = ingredient.kind;
        for (int i = 0; i < ingredientSprites.Length; i++)
        {
            if (ingredientSprites[i].kind == kind)
            {
                if (ingredient is IChoppable)
                {
                    IChoppable choppable = (IChoppable)ingredient;
                    if (choppable.IsChopped())
                    {
                        return ingredientSprites[i].chopped;
                    }
                }

                return ingredientSprites[i].single;
            }
        }

        return null;
    }

    public Sprite GetIngredientStockSprite(Ingredient.Kind kind)
    {
        for (int i = 0; i < ingredientSprites.Length; i++)
        {
            if (ingredientSprites[i].kind == kind)
            {
                return ingredientSprites[i].stock;
            }
        }

        return null;
    }
}

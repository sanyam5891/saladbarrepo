﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreenUIController : ScreenUIController
{
    public Text winner;

    private void OnEnable()
    {
        SessionController.Instance.onGameOver -= UpdateResult;
        SessionController.Instance.onGameOver += UpdateResult;
    }

    private void OnDisable()
    {
        SessionController.Instance.onGameOver -= UpdateResult;
    }

    public void UpdateResult(Result result)
    {
        winner.text = result.name + ": " + result.score; 
    }
}

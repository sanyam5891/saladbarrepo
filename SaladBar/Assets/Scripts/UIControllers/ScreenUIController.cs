﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenUIController : MonoBehaviour
{
    public enum Type
    {
        MAIN_MENU,
        GAME,
        RESULT
    };

    public Type type;
}

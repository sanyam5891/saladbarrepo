﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerUIController : MonoBehaviour
{
    private Customer m_customer;
    private Camera m_camera;
    private RectTransform m_rectTransform;

    public Vector3 offset;
    public Image fillImage;

    private void PositionTimer()
    {
        Vector3 customerScreenPostion = m_camera.WorldToScreenPoint(m_customer.transform.position);
        m_rectTransform.position = customerScreenPostion + offset;
        fillImage.fillAmount = (m_customer.WaitTimeLeft / m_customer.TotalWaitTime);
    }

    public void Init(Customer customer)
    {
        m_customer = customer;
        m_camera = Camera.main;
        m_rectTransform = GetComponent<RectTransform>();
        PositionTimer();
    }

    public void Update()
    {
        if (m_customer)
        {
            PositionTimer();
        }
    }
}

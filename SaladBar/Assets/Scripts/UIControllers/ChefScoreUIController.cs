﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChefScoreUIController : MonoBehaviour
{
    public Text scoreTxt, nameTagTxt, messageTxt;

    public void UpdateData(Chef chef)
    {
        scoreTxt.text = chef.Name + ": " + chef.Score + ", " + ((int)(chef.TimeLeft)) + "s";

        Vector3 chefScreenPos = Camera.main.WorldToScreenPoint(chef.transform.position);
        Vector3 namePos = chefScreenPos + new Vector3(0.0f, 75.0f, 0.0f);
        Vector3 messagePos = chefScreenPos + new Vector3(0.0f, 0.0f, 0.0f);

        nameTagTxt.transform.position = namePos;
        nameTagTxt.text = chef.Name;

        if(chef.IsChopping)
        {
            messageTxt.transform.position = messagePos;
            messageTxt.text = "Chopping";
            messageTxt.gameObject.SetActive(true);
        }
        else
        {
            messageTxt.gameObject.SetActive(false);
        }
    }
}
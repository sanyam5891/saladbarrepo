﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarScreenUIController : ScreenUIController
{
    private Bar m_bar;
    private List<Chef> m_chefs;
    private int m_chefCount;
    private Dictionary<Customer, CustomerUIController> m_customerUIControllersMap;

    public CustomerUIController pfCustomerUIController;
    public List<ChefScoreUIController> chefScoreUIControllers;

    private void Awake()
    {
        m_customerUIControllersMap = new Dictionary<Customer, CustomerUIController>();
    }

    private void OnEnable()
    {
        m_bar = SessionController.Instance.Bar;
        m_chefs = m_bar.Chefs;
        m_chefCount = m_chefs.Count;

        SessionController.Instance.onCustomerEntered -= OnCustomerEntered;
        SessionController.Instance.onCustomerLeft -= OnCustomerLeft;

        SessionController.Instance.onCustomerEntered += OnCustomerEntered;
        SessionController.Instance.onCustomerLeft += OnCustomerLeft;
    }

    private void OnDisable()
    {
        SessionController.Instance.onCustomerEntered -= OnCustomerEntered;
        SessionController.Instance.onCustomerLeft -= OnCustomerLeft;
    }

    private void Update()
    {
        for(int i = 0; i < m_chefCount; i++)
        {
            chefScoreUIControllers[i].UpdateData(m_chefs[i]);
        }
    }

    private void OnCustomerEntered(Customer customer)
    {
        if(!m_customerUIControllersMap.ContainsKey(customer))
        {
            CustomerUIController customerUIController = Instantiate(pfCustomerUIController, transform);
            customerUIController.Init(customer);
            m_customerUIControllersMap.Add(customer, customerUIController);
        }
    }

    private void OnCustomerLeft(Customer customer)
    {
        if(m_customerUIControllersMap.ContainsKey(customer))
        {
            GameObject uiController = m_customerUIControllersMap[customer].gameObject;
            m_customerUIControllersMap.Remove(customer);
            Destroy(uiController);
        }
    }
}

﻿public interface IChoppable
{
    bool IsChopped();
    void SetChopped(bool chopped);
}

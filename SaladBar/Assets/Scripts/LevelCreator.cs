﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class LevelCreator : MonoBehaviour
{
    public enum LevelKind
    {
        EASY,
        MEDIUM,
        HARD
    };
    
    public LevelKind kind;
    public LevelData levelData;
    
    public string GetLevelJson()
    {
        string levelDataJson = JsonUtility.ToJson(levelData);
        return levelDataJson;
    }

    public LevelData GetLevelData()
    {
        return levelData;
    }
}

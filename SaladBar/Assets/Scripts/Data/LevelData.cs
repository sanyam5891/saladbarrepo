﻿using System.Collections.Generic;
using System;

[Serializable]
public struct ChefData
{
    public string name;
    public float movementSpeed;
    public float choppingSpeed;
    public int ingredientCarryCapacity;
};

[Serializable]
public struct BarData
{
    public int seatingCapacity;
    public List<Ingredient.Kind> ingredientTypes;
};

[Serializable]
public struct LevelData
{
    public BarData bar;
    public List<ChefData> chefs;
    public float m_timePerChef;
    public float m_customerMaxWaitTime;
    public int m_minIngredientsPerDish;
    public int m_maxIngredientsPerDish;
};

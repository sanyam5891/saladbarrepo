﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeScreenInputMapper : InputMapper
{
    public GameAction onExit, onPlay;
    public KeyCode exitKey, playKey;

    private void Awake()
    {
        m_actions = new Dictionary<KeyCode, GameAction>();
        m_actions.Add(exitKey, onExit);
        m_actions.Add(playKey, onPlay);
    }
}

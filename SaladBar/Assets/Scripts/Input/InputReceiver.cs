﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum InputKeyStatus
{
    PRESSED,
    PRESSING,
    RELEASED
};
 
/// <summary>
/// This is the primary input receiver for the game.
/// All ScreenInputReceiver(s) subscribe input events from here and relay it to their respective InputMapper(s).
/// </summary>
public class InputReceiver : MonoBehaviour
{
    private Dictionary<KeyCode, InputKeyStatus> m_pressedKeys;
    public static Action<KeyCode> onKeyPressed, onKeyPressing, onKeyReleased;
    
    void Awake()
    {
        m_pressedKeys = new Dictionary<KeyCode, InputKeyStatus>();
    }

    // Update is called once per frame
    void Update()
    {
        List<KeyCode> keysCopy = new List<KeyCode>(m_pressedKeys.Keys);
        foreach (KeyCode key in keysCopy)
        {
            if (Input.GetKeyUp(key))
            {
                onKeyReleased?.Invoke(key);
                m_pressedKeys[key] = InputKeyStatus.RELEASED;
            }
            else if (Input.GetKey(key))
            {
                onKeyPressing?.Invoke(key);
                m_pressedKeys[key] = InputKeyStatus.PRESSING;
            }
            else if (Input.GetKeyDown(key))
            {
                onKeyPressed?.Invoke(key);
                m_pressedKeys[key] = InputKeyStatus.PRESSED;
            }
            else
            {
                m_pressedKeys.Remove(key);
            }
        }

        if (Input.anyKeyDown)
        {
            foreach (KeyCode key in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(key) && !m_pressedKeys.ContainsKey(key))
                {
                    onKeyPressed?.Invoke(key);
                    m_pressedKeys.Add(key, InputKeyStatus.PRESSED);
                }
            }
        }
    }
}

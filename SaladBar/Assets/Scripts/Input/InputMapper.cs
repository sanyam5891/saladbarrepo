﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameAction
{
    public Action onEnter;
    public Action onStay;
    public Action onExit;
}

/// <summary>
/// This is the module that maps the input to its respective "actions".
/// To create different input maps, extend this class.
/// For eg. Pressing Esc on MainMenu will result in a different action than pressing Esc in Game.
/// So create MainMenuScreenInputMapper and GameScreenInputMapper and bind keys to their respective actions.
/// </summary>
public class InputMapper : MonoBehaviour
{
    protected Dictionary<KeyCode, GameAction> m_actions;

    public void OnKeyPress(KeyCode key)
    {
        if (m_actions.ContainsKey(key))
            m_actions[key].onEnter?.Invoke();
    }

    public void OnKeyPressing(KeyCode key)
    {
        if (m_actions.ContainsKey(key))
        {
            m_actions[key].onStay?.Invoke();
        }
    }

    public void OnKeyReleased(KeyCode key)
    {
        if (m_actions.ContainsKey(key))
            m_actions[key].onExit?.Invoke();
    }
}

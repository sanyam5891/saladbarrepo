﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

/// <summary>
/// Each ScreenInputReceiver takes in input events from InputReceiver and relays to InputMapper(s).
/// </summary>
public class ScreenInputReceiver : MonoBehaviour
{
    public List<InputMapper> inputMappers;

    private void OnEnable()
    {
        InputReceiver.onKeyPressed -= OnKeyPressed;
        InputReceiver.onKeyPressing -= OnKeyPressing;
        InputReceiver.onKeyReleased -= OnKeyReleased;

        InputReceiver.onKeyPressed += OnKeyPressed;
        InputReceiver.onKeyPressing += OnKeyPressing;
        InputReceiver.onKeyReleased += OnKeyReleased;
    }

    private void OnDisable()
    {
        InputReceiver.onKeyPressed -= OnKeyPressed;
        InputReceiver.onKeyPressing -= OnKeyPressing;
        InputReceiver.onKeyReleased -= OnKeyReleased;
    }

    private void OnKeyPressed(KeyCode keyCode)
    {
        foreach(InputMapper inputMapper in inputMappers)
        {
            inputMapper.OnKeyPress(keyCode);
        }
    }

    private void OnKeyPressing(KeyCode keyCode)
    {
        foreach (InputMapper inputMapper in inputMappers)
        {
            inputMapper.OnKeyPressing(keyCode);
        }
    }

    private void OnKeyReleased(KeyCode keyCode)
    {
        foreach (InputMapper inputMapper in inputMappers)
        {
            inputMapper.OnKeyReleased(keyCode);
        }
    }
}

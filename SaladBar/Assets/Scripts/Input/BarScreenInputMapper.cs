﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarScreenInputMapper : InputMapper
{
    public GameAction onMoveLeft, onMoveRight, onMoveUp, onMoveDown, onPick, onPlace;
    public KeyCode moveLeftKey, moveRightKey, moveUpKey, moveDownKey, pickKey, placeKey;

    private void Awake()
    {
        onMoveLeft = new GameAction();
        onMoveRight = new GameAction();
        onMoveUp = new GameAction();
        onMoveDown = new GameAction();
        onPick = new GameAction();
        onPlace = new GameAction();

        m_actions = new Dictionary<KeyCode, GameAction>();
        m_actions.Add(moveLeftKey, onMoveLeft);
        m_actions.Add(moveRightKey, onMoveRight);
        m_actions.Add(moveUpKey, onMoveUp);
        m_actions.Add(moveDownKey, onMoveDown);
        m_actions.Add(pickKey, onPick);
        m_actions.Add(placeKey, onPlace);
    }
}

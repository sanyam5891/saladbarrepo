﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public struct Result
{
    public string name;
    public int score;

    public Result(string name, int score)
    {
        this.name = name;
        this.score = score;
    }
}

public class SessionController : MonoBehaviour
{
    public Action<Customer> onCustomerEntered, onCustomerLeft;
    public Action<Result> onGameOver;
    public List<LevelCreator> levelCreators;
    public List<ScreenUIController> screenUIControllers;
    public BarScreenInputMapper[] barScreenInputMappers;
    public Bar pfBar;

    private int m_currentLevel;
    private Bar m_bar;
    private static SessionController m_instance;

    public static SessionController Instance { get => m_instance; }
    public Bar Bar { get => m_bar; }

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        EnableScreen(ScreenUIController.Type.MAIN_MENU);
    }

    private ScreenUIController GetScreenUIController(ScreenUIController.Type type)
    {
        foreach (ScreenUIController screenUIController in screenUIControllers)
        {
            if(screenUIController.type == type)
            {
                return screenUIController;
            }
        }
        return null;
    }

    private void EnableScreen(ScreenUIController.Type type)
    {
        foreach (ScreenUIController screenUIController in screenUIControllers)
        {
            screenUIController.gameObject.SetActive(screenUIController.type == type);
        }
    }

    private void DestroyBar()
    {
        if (m_bar != null)
        {
            m_bar.onCustomerEntered -= OnCustomerEntered;
            m_bar.onCustomerLeft -= OnCustomerLeft;
            m_bar.onGameOver -= OnGameOver;
            Destroy(m_bar.gameObject);
        }
    }

    private void SetupBar(LevelData levelData)
    {
        DestroyBar();

        m_bar = Instantiate(pfBar);
        m_bar.Init(levelData, barScreenInputMappers);

        m_bar.onCustomerEntered -= OnCustomerEntered;
        m_bar.onCustomerLeft -= OnCustomerLeft;
        m_bar.onGameOver -= OnGameOver;

        m_bar.onCustomerEntered += OnCustomerEntered;
        m_bar.onCustomerLeft += OnCustomerLeft;
        m_bar.onGameOver += OnGameOver;

        m_bar.gameObject.SetActive(true);
        m_bar.Begin();

        EnableScreen(ScreenUIController.Type.GAME);
    }

    public void StartLevel(int levelKind)
    {
        m_currentLevel = levelKind;
        LevelCreator.LevelKind kind = (LevelCreator.LevelKind)levelKind;
        LevelData levelData = levelCreators[levelKind].GetLevelData();
        SetupBar(levelData);
    }

    public void OnMainMenu()
    {
        DestroyBar();
        EnableScreen(ScreenUIController.Type.MAIN_MENU);
    }

    public void OnRestart()
    {
        StartLevel(m_currentLevel);
    }

    public void OnGameOver()
    {
        int highestScore = int.MinValue;
        Chef winner = null;
        foreach (Chef chef in m_bar.Chefs)
        {
            if (chef.Score > highestScore)
            {
                winner = chef;
                highestScore = chef.Score;
            }
        }

        EnableScreen(ScreenUIController.Type.RESULT);

        Result result = new Result(winner.Name, winner.Score);
        onGameOver?.Invoke(result);

        DestroyBar();
    }

    public void OnCustomerEntered(Customer customer)
    {
        onCustomerEntered?.Invoke(customer);
    }

    public void OnCustomerLeft(Customer customer)
    {
        onCustomerLeft?.Invoke(customer);
    }
}
